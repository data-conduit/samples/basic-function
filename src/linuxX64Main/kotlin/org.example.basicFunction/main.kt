package org.example.basicFunction

import platform.posix.usleep
import kotlin.system.exitProcess

fun main(args: Array<String>) {
    println("Starting Basic Function...")
    if (args.isNotEmpty() && args.first() == "--infinite-loop") {
        println("Doing infinite loop...")
        while (true) {
            usleep(500u)
        }
    }
    if (args.isNotEmpty() && args.first() == "--exit") exitProcess(7)
    if (args.isNotEmpty() && args.first().isNotEmpty()) {
        args.forEachIndexed { pos, item -> println("Arg ${pos + 1}: $item") }
    }
}
