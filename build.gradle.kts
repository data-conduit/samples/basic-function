group = "org.example"
version = "0.1"

plugins {
    kotlin("multiplatform") version "1.6.21"
}

repositories {
    mavenCentral()
}

kotlin {
    linuxX64 {
        binaries {
            executable("basic_func") {
                entryPoint = "org.example.basicFunction.main"
            }
        }
    }
}
